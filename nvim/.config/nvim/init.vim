let g:python3_host_prog = '/Users/apsummers/.virtualenvs/neovim/bin/python'

call plug#begin('~/.local/share/nvim/plugged')
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1

Plug 'airblade/vim-gitgutter'
Plug 'arcticicestudio/nord-vim'
Plug 'davidhalter/jedi-vim', { 'for': 'python' }
Plug 'ervandew/supertab'
Plug 'itchyny/lightline.vim'
Plug 'majutsushi/tagbar'
Plug 'rizzatti/dash.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'wellle/targets.vim'
call plug#end()

" colorscheme
colorscheme nord

" dev niceities
syntax on
set expandtab
set tabstop=4
set relativenumber
set number
set colorcolumn=80

" basic config
let mapleader = " "

" dash config
nmap <silent> <leader>d <Plug>DashSearch

" lightline.vim
let g:lightline = {
\  'colorscheme': 'nord',
\  'active': {
\    'left': [ [ 'mode', 'paste' ],
\              [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
\  },
\  'component_function': {
\    'gitbranch': 'fugitive#head'
\  },
\}

" supertab config
let g:SuperTabDefaultCompletionType = "<c-n>"

